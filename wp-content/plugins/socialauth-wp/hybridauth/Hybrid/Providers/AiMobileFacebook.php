<?php
/**
 * Hybrid Auth library doesn't work with mobile version of Facebook
 * adding a subclass to deal with it
 */
require_once 'Facebook.php';

class Hybrid_Providers_AiMobileFacebook extends Hybrid_Providers_Facebook {

    function loginBegin() {

        $parameters = array("scope" => $this->scope, "redirect_uri" => $this->endpoint, "display" => "touch");
        $optionals  = array("scope", "redirect_uri", "display");

        foreach ($optionals as $parameter){
            if( isset( $this->config[$parameter] ) && ! empty( $this->config[$parameter] ) ){
                $parameters[$parameter] = $this->config[$parameter];
            }
        }

        $url = str_replace('www.facebook', 'm.facebook', $this->api->getLoginUrl( $parameters ));

        // redirect to facebook
        Hybrid_Auth::redirect( $url );
    }

}
