# This is a basic VCL configuration file for varnish.  See the vcl(7)
# man page for details on VCL syntax and semantics.
#
# Default backend definition.  Set this to point to your content
# server.
#
include "devicedetect.vcl";
include "local.vcl";

backend admin {
    .host = "172.20.23.100";
    .port = "80";
}

acl purge {
        "localhost";
}

sub vcl_recv {
        call devicedetect;
        # req.http.X-UA-Device is copied by Varnish into bereq.http.X-UA-Device

        #tie admin to admin server
        if (req.url ~ "^/wp-admin") {
            set req.backend = admin;
        }

        if (req.url ~ "preview=true"
            || (req.url ~ "wp-(login|admin)" && req.url !~ "postratings") || req.url ~ "postratings-options"
            || (req.url ~ "postratings" && req.url ~ "nonce")
            || req.url ~ "^/(account|profile|register|wp-content/themes/nuevo/ajax|ai-mailchimp/webhook.php)" || req.url ~ "socialauth") {
                return(pass);
        }

        # unless sessionid/csrftoken is in the request, don't pass ANY cookies (referral_source, utm, etc)
        if (req.request == "GET" && (req.url ~ "^/static" || req.url ~ "^/media" || (req.http.cookie !~ "sessionid" && req.http.cookie !~ "csrftoken"))) {
                remove req.http.Cookie;
        }

        if (req.request == "PURGE") {
                if (!client.ip ~ purge) {
                        error 405 "Not allowed.";
                }
                return(lookup);
        }

        if (req.url ~ "^/$") {
               unset req.http.cookie;
        }
}

sub vcl_hit {
        if (req.request == "PURGE") {
                set obj.ttl = 0s;
                error 200 "Purged.";
        }
}

sub vcl_miss {
        if (req.request == "PURGE") {
                error 404 "Not in cache.";
        }
        if (!(req.url ~ "wp-(login|admin)"
                || req.url ~ "^/(account|profile|register)"
                || req.url ~ "socialauth"
                || req.url ~ "admin"
                || req.url ~ "postratings"
                )) {
                unset req.http.cookie;
        }
        if (req.url ~ "^/[^?]+.(jpeg|jpg|png|gif|ico|js|css|txt|gz|zip|lzma|bz2|tgz|tbz|html|htm)(\?.|)$") {
                unset req.http.cookie;
                set req.url = regsub(req.url, "\?.$", "");
        }
        if (req.url ~ "^/$") {
                unset req.http.cookie;
        }
}

sub vcl_fetch {
    if (req.http.X-UA-Device) {
        if (!beresp.http.Vary) { # no Vary at all
            set beresp.http.Vary = "X-UA-Device";
        } elseif (beresp.http.Vary !~ "X-UA-Device") { # add to existing Vary
            set beresp.http.Vary = beresp.http.Vary + ", X-UA-Device";
        }
    }
    # comment this out if you don't want the client to know your classification
    set beresp.http.X-UA-Device = req.http.X-UA-Device;

    set beresp.ttl = 1440m;
    if (req.url ~ "^/static") {
       unset beresp.http.set-cookie;
       return (deliver);
    }

    if (req.url ~ "^/media") {
       unset beresp.http.set-cookie;
       return (deliver);
    }

    # pass through for anything with a session/csrftoken set
    if (beresp.http.set-cookie ~ "sessionid" || beresp.http.set-cookie ~ "csrftoken") {
      return(hit_for_pass);
    } else {
    #   return (deliver);
    }

        if (req.url ~ "^/$") {
                unset beresp.http.set-cookie;
        }
        if (!(req.url ~ "wp-(login|admin)"
                || req.url ~ "^/(account|profile|register)"
                || req.url ~ "socialauth"
                || req.url ~ "admin"
                || req.url ~ "postratings"
                )){
                        unset beresp.http.set-cookie;
        }

}

sub vcl_deliver {
    if ((req.http.X-UA-Device) && (resp.http.Vary)) {
        set resp.http.Vary = regsub(resp.http.Vary, "X-UA-Device", "User-Agent");
    }
}
